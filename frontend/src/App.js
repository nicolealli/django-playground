import React, { Component } from 'react';
import axios from "axios";

import AddTaskForm from "./components/AddTaskForm"

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isFormVisible: false,
      todoItem: {
        title: "",
        description: "",
        completed: false
      },
      todoList: [],
      viewCompleted: false
    }
  }

  componentDidMount() {
    this.fetchList()
  }

  displayCompleted = (status) => {
    return this.setState({ viewCompleted: status })
  }

  fetchList = () => {
    axios
      .get("/api/todo")
      .then(res => this.setState({ todoList: res.data }))
      .catch(err => console.log(err))
  }

  handleAddTask = item => {
    this.setState({ isFormVisible: true })
  }

  handleDelete = item => {
    axios.delete(`/api/todo/${item.id}`).then(() => this.fetchList())
  }

  handleEdit = item => {
    this.setState({
      isFormVisible: true,
      todoItem: item
    })
  }

  refreshList = () => {
    this.fetchList()
    this.setState({
      todoItem: {
        title: "",
        description: "",
        completed: false
      },
      isFormVisible: false
    })
  }

  renderTabList = () => {
    return (
      <ul className="nav nav-tabs">
        <li
          className={this.state.viewCompleted ? "nav-link active" : "nav-link"}
        >
          <button className="btn btn-link" onClick={() => this.displayCompleted(true)}>
            Complete
          </button>
        </li>
        <li
          className={this.state.viewCompleted ? "nav-link" : "nav-link active"}
        >
          <button className="btn btn-link" onClick={() => this.displayCompleted(false)}>
            Incomplete
          </button>
        </li>
      </ul>
    )
  }

  renderItems = () => {
    const { viewCompleted } = this.state
    const newItems = this.state.todoList.filter(item => item.completed === viewCompleted)

    return newItems.map(item => (
      <li
        key={item.id}
        className="list-group-item d-flex justify-content-between align-items-center"
      >
        <span>
          { item.title }
        </span>
        <span>
          { item.description }
        </span>
        <span>
        <button
            className="btn btn-primary mx-3" 
            onClick={() => this.handleEdit(item)}
          >
            Edit
          </button>
          <button
            className="btn btn-danger" 
            onClick={() => this.handleDelete(item)}
          >
            Delete
          </button>
        </span>
      </li>
    ))
  }

  render() {
    return (
      <main className="container">
        <h1 className="my-4 text-center"> Todo </h1>

        <button className="btn btn-primary my-4" onClick={() => this.handleAddTask()}>
          Add Task
        </button>

        { this.renderTabList() }
        <ul className="list-group">
          { this.renderItems() }
        </ul>

        {this.state.isFormVisible && 
          <AddTaskForm todoItem={this.state.todoItem} refreshList={() => this.refreshList()}/>
        }
      </main>
    )
  }
}

export default App;
