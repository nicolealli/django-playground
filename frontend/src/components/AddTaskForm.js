import React, { Component } from "react"
import axios from "axios";

class AddTaskForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            values: this.props.todoItem
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        const value = event.target.type === "checkbox" ? event.target.checked : event.target.value 
        const values = { ...this.state.values, [event.target.name]: value }
        this.setState({ values })
    }

    handleSubmit(event) {
        event.preventDefault()
        const item = Object.assign({}, this.state.values)

        if (item.id) {
            axios
                .put(`/api/todo/${item.id}/`, item)
                .then(() => this.props.refreshList())
        } else {
            axios
                .post("/api/todo/", item)
                .then(() => this.props.refreshList())
        }
    }

    render() {
        return (
            <section>
                <h2 className="mt-4">
                    { this.state.values.id ? "Edit" : "Add" } Task
                </h2>
                <form
                    className="my-4"
                    onSubmit={this.handleSubmit}
                >
                    <label className="form-label">Title</label>
                    <input className="form-control" type="text" name="title" value={this.state.values.title} onChange={this.handleChange} />

                    <label className="form-label">Decription</label>
                    <input className="form-control" type="text" name="description" value={this.state.values.description} onChange={this.handleChange} />
                    
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" name="completed" checked={this.state.values.completed} onChange={this.handleChange} />
                        <label className="form-check-label">completed</label>
                    </div>

                    <button className="btn btn-primary mt-3" onClick={this.handleSubmit} type="submit"> Save </button>
                </form>
            </section>
        )
    }
}

export default AddTaskForm