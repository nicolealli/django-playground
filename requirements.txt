Django>=3.0,<4.0
djangorestframework==3.12.0
django-cors-headers==3.7.0
psycopg2-binary>=2.8
